# Scripts

This repository contains several personal scripts.

## Operating system support

The content of this repository supports any operating system with the abbility of running the associated language or shell. For further information about additional requirements to the operating system study the individual section associated to the script you plan to execute. The scripts contained in this repository are only tested on GNU/Linux distributions.

## Development state

This repository may contain alpha-level software and running outdated versions of it is not advised in any way. Files may be overwritten and permanent damage may apply to the installation of your operating system. Run this at your own risks.

## Contents

Study the information related to the contents of this repository as listed below before executing any of it:

### Dmenu-config

This script aims to simplify the process of quickly editing several of the most common configuration files. Additional configuration files can easily be added by adding them and their path to the declare -a command. Files not listed can be edited too by entering their path in the dmenu prompt.

### Dependencies

The script requires dmenu to be installed on the system, either the version in your distrobution's repository or the version hosted on the suckless website at https://tools.suckless.org/dmenu.

### Dmenu-wall-config

This script aims to simplify the process of setting a wallpaper to your xserver. It offers the choice of several wallpaper packs that can be configured by editing the first declare -a statement and can set a wallpaper at random or at your choice. The script will write to a script called xwallpaper at ~/.config/xwallpaper. You can change the name or location of this file by editing the $xwallcfg variable. The script will be generated based on the $xwallcfg variable by the script. Running the script in your xinitrc will reload the previously set wallpaper at startup.

### Dependencies

The script depends on xwallpaper, sxiv and dmenu to be installed on the system. Check out dmenu at https://tools.suckless.org/dmenu.

## License

The content of this repository is licensed under the GNU General Public License as shown in the LICENSE associated with this repository.
